<?php

namespace app\controllers;

use Yii;
use app\models\Employees;
use app\models\Birthday;
use yii\web\Controller;
use yii\helpers\json;


class SiteController extends Controller
{
     //вывод списка всех сторудников
    public function actionIndex()
    {
        $employees = Employees::getEmployees();
        return $this->render('index',compact('employees'));
    }
    //возвращает список сотрудников с сортировкой в зависимости от выбора поля, по которому происходит сортировка
    public function actionSort()
    {

        $post = Yii::$app->request->post();
        $data =  Json::decode(Json::encode($post), true);

        $strAppend = ' ORDER BY '.$data['property'].' '.$data['typeSort'];
        $employees = Employees::getEmployees($strAppend);

        return json_encode($employees);
    }
    //поиск сотрудника по id для его изменения или удаления
    public function actionFind()
    {
        $post = Yii::$app->request->post();
        $data =  Json::decode(Json::encode($post), true);

        $model = Employees::find()
        ->where(['id' => $data['id_user']])
        ->AsArray()
        ->one();
        $birthday = Birthday::find()
        ->where(['id_user' =>  $data['id_user']])
        ->AsArray()
        ->one();
        $model['birthday'] = $birthday['birthday'];

        return json_encode($model);

    }
    //добавляет нового сотрудника
    public function actionAdd()
    {   

        $post = Yii::$app->request->post();
        $data =  Json::decode(Json::encode($post), true);

        $birthday = new Birthday();
        $employees = new Employees();


        $employees->first_name = $post['first_name'];
        $employees->middle_name = $post['middle_name'];
        $employees->last_name = $post['last_name'];
        $birthday->birthday = $post['birthday'];
     
        if($employees->validate() & $birthday->validate()){

            $employees->first_name = $data['first_name'];
            $employees->middle_name = $data['middle_name'];
            $employees->last_name = $data['last_name'];
            $employees->save();

            $birth = date($data['birthday']);

            
            $birthday->id_user = $employees->id;
            $birthday->birthday = $birth;
            $birthday->save();

            $res = json_encode($post);
            return $res;

        }else{

            return false;

        }

    }
    //удаляет сорудникво из бд
    public function actionDel()
    {
        $post = Yii::$app->request->post();
        $data =  Json::decode(Json::encode($post), true);

        $del_employee = Employees::findOne($data['id_user']);
        $del_employee->delete();

        $del_employee_birthday = Birthday::find()->where(['id_user'=>$data['id_user']])->one();
        $del_employee_birthday->delete();

        $res = json_encode($data);
        return $res;
    }
    //изменение сотрудника
    public function actionEdit()
    {
        $post = Yii::$app->request->post();
        $data = Json::decode(Json::encode($post), true);

        $birthday = new Birthday();
        $employees = new Employees();


        $employees->first_name = $post['first_name'];
        $employees->middle_name = $post['middle_name'];
        $employees->last_name = $post['last_name'];
        $birthday->birthday = $post['birthday'];
        $str = $employees->first_name. ' '. $employees->middle_name.' '. $employees->last_name. ' '. $birthday->birthday;

     
        if($employees->validate() & $birthday->validate()){

            $editing_employee = Employees::findOne($data['id_user']);
            $editing_employee->first_name = $data['first_name'];
            $editing_employee->middle_name = $data['middle_name'];
            $editing_employee->last_name = $data['last_name'];
            $editing_employee->save();

            $editing_birth = Birthday::find()->where(['id_user'=>$data['id_user']])->one();
            $editing_birth->birthday = date($data['birthday']);
            $editing_birth->save();

            
            $res = json_encode($data);
            return $res;
           

        }else{

            return false;

        }

    }

}

  
