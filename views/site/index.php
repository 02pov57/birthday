<?php



$this->title = 'Главная страница';

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\modalWindow\modalWindow;
use app\assets\js;

?>
<div class="site-index">

    <div class="jumbotron">
       <h3>Учет дней рождения сотрудников организации</h3>
    </div>

    <div class="body-content">

        <div class="modal" id="modal-info" style="display: none; padding-right: 17px;">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
                

              </div>
              <div class="modal-footer">

              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <table class="table table-striped main-table">
            <thead>
                <tr>
                    <th id="last_name_sort" data-property='last_name' data-sort="asc">Фамилия</th>
                    <th>Имя</th>
                    <th>Отчество</th>
                    <th id="birthday_sort" data-property='birthday' data-sort="asc">Дата рождения</th>
                </tr></thead>
            <?php 

            foreach ($employees as $employee) { ?>
                
                <tr>

                    <td><? echo $employee['last_name']; ?></td>
                    <td><? echo $employee['first_name']; ?></td>
                    <td><? echo $employee['middle_name']; ?></td>
                    <td><? echo $employee['birthday']; ?></td>
                    <td><button class="btn btn-success edit_modal" name="edit" id=<? echo $employee['id_user']?>>Изменить</button></td>
                    <td><button class="btn btn-danger del_modal" name="del" id=<? echo $employee['id_user']?>>Удалить</button></td>
                </tr>

        <?    } 

          

        ?>
        </table>
     
       <?
        $params = Yii::$app->params['add'];

       echo  Html::submitButton('Добавить', ['class' => 'btn btn-info add_modal']);

       ?>

    </div>

</div>
