-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Фев 04 2020 г., 20:45
-- Версия сервера: 5.5.53
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `birthday`
--
CREATE DATABASE IF NOT EXISTS `birthday` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `birthday`;

-- --------------------------------------------------------

--
-- Структура таблицы `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `birthday` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `birthday`
--

INSERT INTO `birthday` (`id`, `id_user`, `birthday`) VALUES
(1, 1, '1993-07-31'),
(2, 2, '1986-06-13'),
(3, 3, '1999-10-18'),
(9, 108, '1995-10-12'),
(10, 109, '1989-07-29'),
(11, 110, '2002-06-13'),
(12, 111, '1956-07-15'),
(13, 112, '1967-06-15');

-- --------------------------------------------------------

--
-- Структура таблицы `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` char(50) NOT NULL,
  `middle_name` char(50) NOT NULL,
  `last_name` char(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `middle_name`, `last_name`) VALUES
(1, 'Артём', 'Игоревич', 'Алфёров'),
(2, 'Иван', 'Николаевич', 'Иванов'),
(3, 'Petr', 'Петрович', 'Петров'),
(108, 'Евгений', 'Евгеньевич', 'Ветров'),
(109, 'Ольга', 'Викторовна', 'Рыбкина'),
(110, 'Иван', 'Николаевич', 'Сидоров'),
(111, 'Дмитрий', 'Иванович', 'Менделеев'),
(112, 'Константин', 'Николаевич', 'Дворецкий');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT для таблицы `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
