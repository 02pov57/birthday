
//отправка данных о новом сотруднике для последующей его записи и добавления в БД 
$('body').on('click', '.add_empl', function(){

	if(!birthdateValidation('add_birthday') || !requiredValidation()){
		return false;
	}else{

		$.ajax({
		  type: 'POST',
		  url: '/add',
		  dataType: 'json',
		  data: {
			  		'first_name': $('#add_first_name').val(),
			  		'middle_name' : $('#add_middle_name').val(),
			  		'last_name': $('#add_last_name').val(),
			  		'birthday' : $('#add_birthday').val(),
		  		 },
		  error: function(req, text, error){ // отслеживание ошибок во время выполнения ajax-запроса

	        alert('Хьюстон, У нас проблемы! ' + text + ' | ' + error);
	      },
		  success: function(res){
		 	$('#modal-info').modal('hide');
		 	location.reload();
		  }
		  
		});
	}

});


// передает данные в контроллер для удаления выбранного сотрудника
$('body').on('click', '.del_empl', function(){

	$.ajax({
	  type: 'POST',
	  url: '/del',
	  dataType: 'json',
	  data: {
		  		'id_user': $(this).attr('id'),
	  		 },

	  success: function(res){
	  		$('#modal-info').modal('hide');
	  		location.reload();
	  }
	  
	});
});

//открывает модальное окно для внесения данных о новом сотруднике
$('.add_modal').on('click', function(){
      
    $('.modal-body').empty();
    $('#modal-info').modal('show');

  	$('.modal-body').append('<form class="edit_form" action='/' method="post">');
  	$('.modal-body').append('<div class= form-group><label class="control-label" for="first_name">Имя</label>');
  	$('.modal-body').append('<input required type="text" name="first_name" id="add_first_name" class="form-control"></div>');
  	$('.modal-body').append('<div class="help-block"></div>');
  	$('.modal-body').append('<div class= form-group><label class="control-label">Отчество</label>');
  	$('.modal-body').append('<input type="text" name="middle_name" id="add_middle_name" class="form-control"></div>');
  	$('.modal-body').append('<div class="help-block"></div>');
  	$('.modal-body').append('<div class= form-group><label class="control-label">Фамилия </label>');
  	$('.modal-body').append('<input type="text" name="last_name" id="add_last_name" class="form-control">');
  	$('.modal-body').append('<div class="help-block"></div>');
  	$('.modal-body').append('<div class= form-group><label class="control-label">Дата рождения</label>');
    $('.modal-body').append('<input type="date" name="birthday" id="add_birthday" class="form-control"></div>');
  	$('.modal-body').append('<div class="help-block"></div></div></form>');
  	$('.modal-body').append('<submit class="btn btn-success add_empl">Добавить </submit>');
  	$('.modal-body').append('<button class="btn btn-default pull-right" data-dismiss="modal">Отмена </button>');

});

// открывает модальное окно для изменения или удаления данных о текущем сотруднике
$('body').on('click', '.edit_modal, .del_modal', function(){
      
    $('.modal-body').empty();
    var actionType = this.name;
    $('#modal-info').modal('show');
    var data = $(this).attr('id');

	$.ajax({
	  async: true,
	  type: 'POST',
	  url: '/find',
	  dataType: 'json',
	  data: {
	  			'id_user': $(this).attr('id'),
		  		
	  		},
	  error: function(req, text, error){ // отслеживание ошибок во время выполнения ajax-запроса

	    alert('Хьюстон, У нас проблемы! ' + text + ' | ' + error);
	  },
	  success: function(res){

	  		if(actionType == 'edit'){

			  	$('.modal-body').append('<form class="edit_form" action='/' method="post">');
			  	$('.modal-body').append('<div class= form-group><label class="control-label" for="first_name'+res['id']+'">Имя</label>');
			  	$('.modal-body').append('<input required type="text" name="first_name" id="first_name'+res['id']+'" class="form-control"  value='+res['first_name']+'></div>');
			  	$('.modal-body').append('<div class="help-block"></div>');
			  	$('.modal-body').append('<div class= form-group><label class="control-label">Отчество</label>');
			  	$('.modal-body').append('<input type="text" name="middle_name" id="middle_name'+res['id']+'" class="form-control"  value='+res['middle_name']+'></div>');
			  	$('.modal-body').append('<div class="help-block"></div>');
			  	$('.modal-body').append('<div class= form-group><label class="control-label">Фамилия </label>');
			  	$('.modal-body').append('<input type="text" name="last_name" id="last_name'+res['id']+'" class="form-control"  value='+res['last_name']+'>');
			  	$('.modal-body').append('<div class="help-block"></div>');
			  	$('.modal-body').append('<div class= form-group><label class="control-label">Дата рождения</label>');
			    $('.modal-body').append('<input type="date" name="birthday" id="birthday'+res['id']+'" class="form-control"  value='+res['birthday']+'></div>');
			  	$('.modal-body').append('<div class="help-block"></div></div></form>');
			  	$('.modal-body').append('<submit class="btn btn-success edit_empl" id='+res['id']+'>Изменить </submit>');
			  	$('.modal-body').append('<button class="btn btn-default pull-right" data-dismiss="modal">Отмена </button>');

	  		}else{

	  			$('.modal-body').append('<center><h4>Вы действиельно хотите удалить сотрудника </h4><h3>'+res['first_name']+' '+res['middle_name']+' '+res['last_name']+'</h3><h4> из списка сотрудников?</h4></center>')
	  			$('.modal-body').append('</br><submit class="btn btn-danger del_empl" id='+res['id']+'>Удалить </submit>');
			  	$('.modal-body').append('<button class="btn btn-default pull-right" data-dismiss="modal">Отмена </button>');
	  		}


	  }
	  
	});

      
});

//отправляет данные в контроллер для изменения данных выбранного сотрудника
$('body').on('click', '.edit_empl', function(){
	
	var id_user = $('.edit_empl').attr('id');


	if(!birthdateValidation('birthday'+id_user) || !requiredValidation()){
		
		return false;
	}else{
		var ress = requiredValidation();
		alert(ress);
		$.ajax({
		  type: 'POST',
		  url: '/edit',
		  dataType: 'json',
		  data: {
		  			'id_user': $(this).attr('id'),
			  		'first_name': $('#first_name'+id_user).val(),
			  		'middle_name' : $('#middle_name'+id_user).val(),
			  		'last_name': $('#last_name'+id_user).val(),
			  		'birthday' : $('#birthday'+id_user).val(),
			  		
		  		 },
		  error: function(req, text, error){ // отслеживание ошибок во время выполнения ajax-запроса

	        alert('Хьюстон, У нас проблемы! ' + text + ' | ' + error);
	      },
		  success: function(res){
		  		$('.modal').modal('hide');
		  		location.reload();
		  }
		  
		});
	}
});

//отправляет запрос в контроллер на сортировку списка по фамилии или дате рождения (в зависимости от выбора)
$('#last_name_sort, #birthday_sort').on('click', function(){

	var typeSort = $(this).attr('data-sort');
	var property = $(this).attr('data-property');
	var id_property = $(this).attr('id');
	//alert(typeSort);
	$.ajax({
	  type: 'POST',
	  url: '/sort',
	  dataType: 'json',
	  data: {
	  			'property': property,
		  		'typeSort': typeSort,
	
	  		 },
	  error: function(req, text, error){ // отслеживание ошибок во время выполнения ajax-запроса

        alert('Хьюстон, У нас проблемы! ' + text + ' | ' + error);
      },
	  success: function(res){
	  		
  		$('.main-table').find("tr:gt(0)").remove();
	  	$.each(res, function(item, value){
  		$('.main-table').append('<tr><td>'+value['last_name']+'</td><td>'+value['first_name']+'</td><td>'+value['middle_name']+'</td><td>'+value['birthday']+'</td><td><button class="btn btn-success edit_modal" id='+value['id_user']+'>Изменить</button></td><td><button class="btn btn-danger del_modal" id='+value['id_user']+'>Удалить</button></td></tr>');

	  	});
	  	var typeSortt = (typeSort == 'asc') ? 'desc' : 'asc';

	  	$('#'+id_property).attr('data-sort', typeSortt);
	  		
	  }
	  
	});

});

//функция, которая проверяет  корректность ввода даты на клиенте
function birthdateValidation(param){

	var birthdate = $('#'+param).val().split('-');
	var dateValid = new Date(birthdate[0]+'/'+birthdate[1]+'/'+birthdate[2]);
	console.log(dateValid);
	
	if (birthdate[0] != dateValid.getFullYear() || birthdate[1] != (dateValid.getMonth() + 1) || birthdate[2] != dateValid.getDate() || birthdate[0] >2019) {

		alert('Дата введена некорректно');

		return false;
	}else{
		return true;
	}
}

//функция проверяет заполнены ли обязательные поля
function requiredValidation(param){

	var data = [];
	var paramValues = {'first_name': 'Имя',
						'middle_name': 'Отчество',
						'last_name': 'Фамилия',
						'birthday': 'Дата',};
	var patt = new RegExp('^[a-zA-zА-Яа-я]$');
	var res = true
	$('#modal-info').find('input', 'date').each(function(){
		data[this.name] = $(this).val();

		if($(this).val() == ''){
			alert('Значение поля '+paramValues[this.name] + ' не должно быть пустым!');

			res = false;
		};

	});
	return res;
	
	
};
