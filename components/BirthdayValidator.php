<?

namespace app\components;

use Yii;
use yii\validators\Validator;

class BirthdayValidator extends Validator
{
	public function init()
	{
		parent::init();

        if ($this->message === null) {
            $this->message = Yii::t('yii', 'date invalid');
        }

	}



	public function validateAttribute($model, $attribute)
	{
		$this->addError($model, $attribute, $this->message);
		return;
	}
}