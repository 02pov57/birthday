<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'add' => [
		'header'=>'Добавить сотрудника',
		'tooglebutton' => [
			'label' => 'Добавить',
			'class' => 'btn btn-info alfer',
		],
		'url' => '\views\add\index.php',
	],
	'del' => [
		'header' => 'Удалить запись о сотруднике ',
		'tooglebutton' => [
			'label' => 'Удалить',
			'class' => 'btn btn-danger',
		],

		'url' => '\views\del\index.php',
	],
	'edit' => [
		'header' => 'Изменить данные сотрудника ',
		'tooglebutton' => [
			'label' => 'Изменить',
			'class' => 'btn btn-success',
		],

		'url' => '\views\edit\index.php',
	],
];
