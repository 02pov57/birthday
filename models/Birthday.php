<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class Birthday extends ActiveRecord
{
	public static function tableName()
	{
		return '{{birthday}}';
	}

	public function rules()
	{
		return[
			
			['birthday', 'required', 'message' => 'Пожалуйста, введите дату рождения'],
			['birthday', 'date', 'format' => 'php:Y-m-d', 'message' => 'введите корректную дату рождения'],

		];
	}
}