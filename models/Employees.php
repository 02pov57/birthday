<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class Employees extends ActiveRecord
{

	public static function tableName()
    {
        return '{{employees}}';
    }

/*
*возвращает список всех сотрудников
*@return array
*/
	public static function getEmployees($strAppend = Null)
	{

		$sql = "SELECT * FROM employees LEFT JOIN birthday on birthday.id_user = employees.id".$strAppend;
		return Yii::$app->db->createCommand($sql)->queryAll();

	}

//добавляет сотрудника
	public function addEmployee($data)
	{
		
		$param = $data;

		$sql = "INSERT INTO employees (first_name, middle_name, last_name) VALUES ('".$data['first_name']."', '".$data['middle_name']."', '".$data['last_name']."')";
		
		$res = Yii::$app->db->createCommand($sql)->execute();

		return true;	

	}

	public function rules()
	{
		return[
			[['first_name', 'middle_name', 'last_name'], 'safe'],
			['first_name', 'required', 'message'=> 'Пожалуйста, введите Имя'],
			['middle_name', 'required', 'message'=> 'Пожалуйста, введите Отчество'],
			['last_name', 'required', 'message' => 'Пожалуйста, введите Фамилию'],

		];
	}

}

